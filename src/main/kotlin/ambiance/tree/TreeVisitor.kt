package ambiance.tree

import ambiance.tree.TreePrintOption.DIRECTORIES
import ambiance.tree.TreePrintOption.DIRECTORIES_AND_FILES
import ambiance.tree.TreePrintOption.FILES
import java.io.IOException
import java.nio.file.FileVisitResult
import java.nio.file.Path
import java.nio.file.SimpleFileVisitor
import java.nio.file.attribute.BasicFileAttributes

class TreeVisitor(private val treePrintOption: TreePrintOption) : SimpleFileVisitor<Path>() {

    private val treeAccumulator = mutableListOf<String>()

    private var level = 0

    override fun visitFile(file: Path?, attrs: BasicFileAttributes?): FileVisitResult {
        if (treePrintOption == FILES || treePrintOption == DIRECTORIES_AND_FILES) {
            treeAccumulator += indentToLevel(file!!.fileName.toString())
        }
        return super.visitFile(file, attrs)
    }

    override fun postVisitDirectory(dir: Path?, exc: IOException?): FileVisitResult {
        level--
        return super.postVisitDirectory(dir, exc)
    }

    override fun preVisitDirectory(dir: Path?, attrs: BasicFileAttributes?): FileVisitResult {
        if (treePrintOption == DIRECTORIES || treePrintOption == DIRECTORIES_AND_FILES) {
            treeAccumulator += indentToLevel(dir!!.fileName.toString())
        }
        level++
        return super.preVisitDirectory(dir, attrs)
    }

    private fun indentToLevel(entry: String): String {
        var indented = entry
        for (i in 0 until level) {
            indented = "  $indented"
        }
        return indented
    }

    fun tree() = treeAccumulator.joinToString("\n")
}