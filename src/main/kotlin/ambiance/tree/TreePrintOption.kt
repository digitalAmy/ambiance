package ambiance.tree

enum class TreePrintOption {

    DIRECTORIES, FILES, DIRECTORIES_AND_FILES
}