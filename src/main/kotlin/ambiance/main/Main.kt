package ambiance.main

import ambiance.bot.DiscordEventListener
import ambiance.bot.function.music.Add
import ambiance.bot.function.music.Clear
import ambiance.bot.function.music.Loop
import ambiance.bot.function.music.Ls
import ambiance.bot.function.music.Next
import ambiance.bot.function.music.Play
import ambiance.bot.function.music.Shuffle
import ambiance.bot.function.music.State
import ambiance.bot.function.music.TogglePause
import ambiance.bot.function.music.Tree
import ambiance.bot.function.music.Volume
import ambiance.bot.function.player.Death
import ambiance.bot.function.player.PlayerInfo
import ambiance.bot.function.player.Register
import ambiance.bot.function.player.Unregister
import ambiance.bot.function.resulthandler.SoundEffectResultHandler
import ambiance.bot.function.roleplaying.Cast
import ambiance.bot.function.roleplaying.ShadowrunCast
import ambiance.bot.function.sfx.LsSfx
import ambiance.bot.function.sfx.Sfx
import ambiance.bot.function.system.Help
import ambiance.bot.function.system.Join
import ambiance.entities.PLAYERS_LOCATION
import ambiance.entities.Player
import ambiance.music.MusicManager
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import discord4j.core.DiscordClient
import discord4j.core.DiscordClientBuilder
import discord4j.core.`object`.entity.Guild
import discord4j.core.event.domain.message.MessageCreateEvent
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Files
import java.util.concurrent.ConcurrentHashMap
import java.util.function.BiPredicate

fun main(args: Array<String>): Unit = mainBody {
    ArgParser(args).parseInto(::AmbianceArgs).run {
        // CLI management
        MUSIC_DIRECTORY = musicDirectory
        SOUND_EFFECTS_DIRECTORY = soundEffectsDirectory
        DICE_THROW_EFFECT_NAME = diceThrowEffectName

        LOG.info("working on MUSIC_DIRECTORY: $MUSIC_DIRECTORY, SOUND_EFFECTS_DIRECTORY: $SOUND_EFFECTS_DIRECTORY")
        AudioSourceManagers.registerLocalSource(PLAYER_MANAGER)

        val client = DiscordClientBuilder(token).build()
        prepare(client)
        client.login().block()
    }
}

private fun prepare(client: DiscordClient) {
    LOG.debug("setting up listener and MusicManagers")
    client.guilds.toStream().forEach { MUSIC_MANAGERS[it] = MusicManager(PLAYER_MANAGER, client) }
    val eventListener = DiscordEventListener()
    client.eventDispatcher.on(MessageCreateEvent::class.java).subscribe(eventListener::onMessageReceived)

    if (CONFIG_LOCATION.toFile().exists()) {
        JSON_MAPPER.readValue(CONFIG_LOCATION.toFile(), Array<AmbianceConfig>::class.java).forEach { ambianceConfig ->
            val musicManager = MUSIC_MANAGERS[MUSIC_MANAGERS.keys.find { it.id.asLong() == ambianceConfig.guildId }!!]!!
            musicManager.scheduler.shuffle = ambianceConfig.shuffle
            musicManager.scheduler.loop = ambianceConfig.loop
            musicManager.scheduler.tracks = ambianceConfig.toPlay.toMutableList()
        }
    }

    Files.find(SOUND_EFFECTS_DIRECTORY.toPath(), 100, BiPredicate { _, attributes -> attributes.isRegularFile }).forEach {
        PLAYER_MANAGER.loadItem(it.toAbsolutePath().toString(), SoundEffectResultHandler())
    }

    Runtime.getRuntime().addShutdownHook(Thread {
        shutdown(client)
    })

    if (PLAYERS_LOCATION.toFile().exists()) {
        PLAYERS.addAll(JSON_MAPPER.readValue(PLAYERS_LOCATION.toFile(), Array<Player>::class.java))
    }
}

private fun shutdown(client: DiscordClient) {
    LOG.debug("shutting down...")
    client.logout().block()
    val ambianceConfigs = MUSIC_MANAGERS
            .onEach { it.value.player.stopTrack() }
            .map { entry -> AmbianceConfig(entry.key.id.asLong(), entry.value.scheduler.shuffle, entry.value.scheduler.shuffle, entry.value.scheduler.tracks) }.toTypedArray()
    JSON_MAPPER.writeValue(CONFIG_LOCATION.toFile(), ambianceConfigs)
    JSON_MAPPER.writeValue(PLAYERS_LOCATION.toFile(), PLAYERS.toTypedArray())
}

val LOG = LoggerFactory.getLogger("ambiance")!!
lateinit var MUSIC_DIRECTORY: File
lateinit var SOUND_EFFECTS_DIRECTORY: File
lateinit var DICE_THROW_EFFECT_NAME: String

val MESSAGE_FUNCTIONS = listOf(
        Help(),
        Join(),
        Play(),
        Add(),
        TogglePause(),
        Next(),
        Clear(),
        Cast(),
        ShadowrunCast(),
        State(),
        Loop(),
        Shuffle(),
        Volume(),
        Ls(),
        Tree(),
        LsSfx(),
        Sfx(),
        Register(),
        Unregister(),
        Death(),
        PlayerInfo()
)

val MUSIC_MANAGERS = mutableMapOf<Guild, MusicManager>()

val SOUND_EFFECTS = ConcurrentHashMap<String, AudioTrack>()
val JSON_MAPPER: ObjectMapper = ObjectMapper(JsonFactory()).enable(SerializationFeature.INDENT_OUTPUT)

val PLAYER_MANAGER = DefaultAudioPlayerManager()

val PLAYERS = mutableListOf<Player>()