package ambiance.entities

import java.io.File

val PLAYERS_LOCATION = File("./players.json").toPath()

data class Player(val id: Long = 0, val deathCounterData: DeathCounterData = DeathCounterData())