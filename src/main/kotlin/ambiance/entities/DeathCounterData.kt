package ambiance.entities

data class DeathCounterData(var deaths: Int = 0)

val DEATH_MESSAGES = listOf(
        "died. It was inevitable.",
        "bit the dust.",
        "was eaten by a grue.",
        "hugged a creeper.",
        "should've rolled.",
        "needs to git gud.",
        "went to a happy place.",
        "is manipulating RNG.",
        "got the trick first try.",
        "death-warped."
)