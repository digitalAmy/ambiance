package ambiance.music.playlist

import java.io.File
import java.nio.file.Path

/**
 * Defines a simple playlist containing paths to files. Used to read playlists
 * as JSON.
 */
class AmbiancePlaylist {

    private var tracks = mutableListOf<String>()

    var isShuffle: Boolean = false
    var isLoop: Boolean = false

    /**
     * Use a basePath to get a list of complete [Files][File] from the track
     * list.
     *
     * @param basePath
     * @return
     */
    fun tracksAsFiles(basePath: Path): List<File> = tracks.map { basePath.resolve(it).toFile() }
}
