package ambiance.music

import com.sedmelluq.discord.lavaplayer.track.AudioTrack

data class ParkedTrack(val track: AudioTrack, val position: Long)