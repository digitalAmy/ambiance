package ambiance.music

import ambiance.bot.function.resulthandler.PlayImmediateResultHandler
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason
import discord4j.core.DiscordClient
import java.io.File
import java.nio.file.Paths
import java.util.Random

/**
 * Handles all scheduling of tracks: when to play what track and when to refill
 * the track list when looping.
 */
class PlaybackScheduler(private val client: DiscordClient, private val player: AudioPlayer, private val manager: AudioPlayerManager, var loop: Boolean = true, var shuffle: Boolean = true) : AudioEventAdapter() {

    var tracks = mutableListOf<File>()
        set(tracks) {
            // sets the overall track list; also overwrites the notPlayed list
            field = tracks
            notPlayed = tracks
        }

    var notPlayed = mutableListOf<File>()
    var parkedTrack: ParkedTrack? = null

    init {
        notPlayed = tracks
    }

    /**
     * Play the given [AudioTrack]. Will resume playing the previously playing song after finished.
     */
    fun playEffect(effect: AudioTrack) {
        if (parkedTrack == null && player.playingTrack != null) {
            parkedTrack = ParkedTrack(player.playingTrack.makeClone(), player.playingTrack.position)
        }
        player.playTrack(effect.makeClone())
    }

    override fun onTrackEnd(player: AudioPlayer?, track: AudioTrack?, endReason: AudioTrackEndReason?) {
        if (endReason == AudioTrackEndReason.FINISHED && parkedTrack != null) {
            player!!.playTrack(parkedTrack!!.track)
            player.playingTrack.position = parkedTrack!!.position
            parkedTrack = null
        } else if (endReason!!.mayStartNext && nextAvailable()) {
            val next = popTrack()

            manager.loadItem(next.absolutePath, PlayImmediateResultHandler(client, player!!))
        }
    }

    /**
     * Sets the overall track list. notPlayed will be kept and just appended.
     *
     * @param tracks
     */
    fun appendTracks(tracks: List<File>) {
        this.tracks.addAll(tracks)
        notPlayed.addAll(tracks)
    }

    /**
     * Checks whether calling [nextTrack] is viable
     *
     * @return true iff there are tracks in [.notPlayed] or loop is enabled
     * with overall tracks available
     */
    private fun nextAvailable(): Boolean {
        return notPlayed.size > 0 || loop && notPlayed.isEmpty() && !tracks.isEmpty()
    }

    private fun popTrack(): File {
        if (notPlayed.size == 0 && loop) {
            notPlayed = tracks
        }

        return if (shuffle) {
            notPlayed.removeAt(RNG.nextInt(notPlayed.size))
        } else {
            notPlayed.removeAt(0)
        }
    }

    /**
     * Order the AudioPlayer to start playback of the next track.
     */
    fun nextTrack() {
        val track = popTrack()
        manager.loadItem(track.absolutePath, PlayImmediateResultHandler(client, player))
    }

    companion object {

        private val RNG = Random()

        /**
         * Get a human readable name for a given [AudioTrack].
         *
         * @param track
         * @return readable name
         */
        fun readableName(track: AudioTrack): String {
            return if (track.info.title.isNullOrEmpty() || track.info.title == "Unknown title") {
                Paths.get(track.identifier).fileName.toString()
            } else {
                track.info.title
            }
        }

        /**
         * Get a human readable name for a given [File].
         *
         * @param track
         * @return readable name
         */
        fun readableName(track: File): String {
            return track.name
        }
    }
}