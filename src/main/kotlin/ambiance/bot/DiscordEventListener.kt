package ambiance.bot

import ambiance.main.LOG
import ambiance.main.MESSAGE_FUNCTIONS
import discord4j.core.event.domain.message.MessageCreateEvent

const val ORDER_PREFIX = "/"

class DiscordEventListener {

    fun onMessageReceived(event: MessageCreateEvent) {
        if (event.message.content.isPresent && event.message.content.get().startsWith(ORDER_PREFIX)) {
            val tokenized = TokenizedMessageReceivedEvent(event)
            LOG.info("received tokens " + tokenized.tokens.joinToString(","))
            val notEmpty = MESSAGE_FUNCTIONS.filter {
                tokenized.tokens[0] == ORDER_PREFIX + it.order
            }.map { it.accept(tokenized) }.isNotEmpty()
            if (notEmpty) {
                event.message.delete("interpreted by bot").subscribe()
            }
        } else {
            LOG.info("received message ${event.message} is not a valid order")
        }
    }
}