package ambiance.bot

import ambiance.main.MUSIC_MANAGERS
import discord4j.core.event.domain.message.MessageCreateEvent

// TODO: rename?
class TokenizedMessageReceivedEvent(val base: MessageCreateEvent) {

    val tokens = base.message.content.get().split(" ".toRegex()).filter { it.isNotEmpty() }.toTypedArray()
    // TODO: Mono stuff
    val musicManager = MUSIC_MANAGERS[base.guild.block()!!]!!
}