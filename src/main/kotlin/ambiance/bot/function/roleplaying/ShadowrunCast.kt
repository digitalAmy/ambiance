package ambiance.bot.function.roleplaying

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction
import ambiance.main.DICE_THROW_EFFECT_NAME
import ambiance.main.SOUND_EFFECTS
import java.util.Random

class ShadowrunCast : BotFunction {

    override val order = "srcast"

    private val rng: Random = Random()

    override fun accept(event: TokenizedMessageReceivedEvent) {
        if (event.tokens.size != 2) {
            answer(event, "usage: ${description()}")
            return
        }

        try {
            if (SOUND_EFFECTS[DICE_THROW_EFFECT_NAME] != null) {
                event.musicManager.scheduler.playEffect(SOUND_EFFECTS[DICE_THROW_EFFECT_NAME]!!)
            }

            val numDice = Integer.parseInt(event.tokens[1])
            if (numDice < 1) {
                answer(event, "can only throw a positive amount of dice")
                return
            }

            var successes = 0
            var failures = 0
            for (i in 0 until numDice) {
                val thrown = rng.nextInt(6) + 1
                if (thrown > 4) {
                    successes++
                } else if (thrown == 1) {
                    failures++
                }
            }

            val response = StringBuilder("$successes successes and $failures failures.")

            if (failures > numDice / 2) {
                if (successes > 0) {
                    response.append(" GLITCH")
                } else {
                    response.append(" CRITICAL GLITCH")
                }
            }

            answer(event, response.toString())
        } catch (nfe: NumberFormatException) {
            answer(event, "${event.tokens[2]} can not be converted to a number")
        }
    }

    override fun description(): String {
        return paddedOrder("$order [numDice]") + "- throw numDice d6 with Shadowrun rules"
    }
}
