package ambiance.bot.function.music

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction

class Clear : BotFunction {

    override val order = "clear"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        event.musicManager.scheduler.tracks = mutableListOf()
    }

    override fun description(): String {
        return paddedOrder(order) + "- clear the current list of tracks"
    }
}
