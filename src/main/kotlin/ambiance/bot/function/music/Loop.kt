package ambiance.bot.function.music

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction

class Loop : BotFunction {

    override val order = "loop"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        event.musicManager.scheduler.loop = java.lang.Boolean.parseBoolean(event.tokens[1].trim { it <= ' ' })
    }

    override fun description(): String {
        return paddedOrder("$order [true|false]") + "- activates/deactivates looping"
    }
}
