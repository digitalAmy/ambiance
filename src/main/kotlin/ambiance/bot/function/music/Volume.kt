package ambiance.bot.function.music

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction

class Volume : BotFunction {

    override val order = "volume"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        try {
            event.musicManager.player.volume = Integer.parseInt(event.tokens[1])
        } catch (nfe: NumberFormatException) {
            answer(event, "${event.tokens[1]} is not a valid integer")
        }
    }

    override fun description(): String {
        return paddedOrder("$order [n]") + "- sets volume of the player to n as percentage"
    }
}
