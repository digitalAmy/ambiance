package ambiance.bot.function.music

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction

class Next : BotFunction {

    override val order = "next"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        event.musicManager.scheduler.nextTrack()
    }

    override fun description(): String {
        return paddedOrder(order) + "- start playing the next track in queue if any"
    }
}
