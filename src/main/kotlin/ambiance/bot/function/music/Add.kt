package ambiance.bot.function.music

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction
import ambiance.main.JSON_MAPPER
import ambiance.main.LOG
import ambiance.main.MUSIC_DIRECTORY
import ambiance.music.playlist.AmbiancePlaylist
import java.io.IOException
import java.nio.file.Paths

class Add : BotFunction {

    override val order = "add"

    override fun description(): String {
        return paddedOrder("$order [path]") + "- add files denoted by path to the playback list"
    }

    override fun accept(event: TokenizedMessageReceivedEvent) {
        val resource = Paths.get(MUSIC_DIRECTORY.toString(), event.tokens[1]).toFile()
        if (resource.exists()) {
            when {
                resource.isDirectory -> // iterate over directory contents
                    event.musicManager.scheduler.appendTracks(resource.listFiles().asList())
                resource.name.matches(".+\\.json".toRegex()) -> {
                    // parse playlist
                    LOG.info("reading a playlist")
                    try {
                        val playlist = JSON_MAPPER.readValue(resource, AmbiancePlaylist::class.java)

                        // iterate over playlist members
                        event.musicManager.scheduler.appendTracks(playlist.tracksAsFiles(MUSIC_DIRECTORY.toPath()))
                        event.musicManager.scheduler.loop = playlist.isLoop
                        event.musicManager.scheduler.shuffle = playlist.isShuffle
                    } catch (ioe: IOException) {
                        LOG.warn("unable to access file $resource", ioe)
                        answer(event, "unable to access file$resource")
                    } catch (e: Exception) {
                        LOG.warn("unable to parse file $resource", e)
                    }
                }
                else -> // normal mechanism
                    event.musicManager.scheduler.tracks = mutableListOf(MUSIC_DIRECTORY.toPath().resolve(event.tokens[1]).toFile())
            }
        }
    }
}
