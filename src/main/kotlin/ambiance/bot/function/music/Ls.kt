package ambiance.bot.function.music

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction
import ambiance.main.MUSIC_DIRECTORY
import java.nio.file.Path

class Ls : BotFunction {

    override val order = "ls"

    override fun description(): String = paddedOrder("$order [path]") + "- list files for given path"

    override fun accept(event: TokenizedMessageReceivedEvent) {
            if (event.tokens.size > 1) {
                val resolved = MUSIC_DIRECTORY.toPath().resolve(event.tokens[1])
                if (resolved.toRealPath().startsWith(MUSIC_DIRECTORY.toPath().toRealPath())) {
                    answer(event, listContents(resolved))
                } else {
                    answer(event, "${event.tokens[1]} not a valid path")
                }
            } else {
                answer(event, listContents(MUSIC_DIRECTORY.toPath()))
            }
    }

    private fun listContents(path: Path): String {
        val relativizedPath = if (path.toAbsolutePath() != MUSIC_DIRECTORY.toPath().toAbsolutePath()) MUSIC_DIRECTORY.toPath().relativize(path).toString() else "<root>"
        var message = "contents of $relativizedPath:\n" + path.toFile().listFiles().joinToString("\n") { it.toRelativeString(path.toFile()) }
        if (message.length > 2000) {
            message = message.take(1997) + "..."
        }
        return message
    }
}