package ambiance.bot.function.music

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction

class TogglePause : BotFunction {

    override val order = "pause"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        event.musicManager.player.isPaused = !event.musicManager.player.isPaused
    }

    override fun description(): String {
        return paddedOrder(order) + "- pause current track"
    }
}
