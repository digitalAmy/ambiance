package ambiance.bot.function.music

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction
import ambiance.music.PlaybackScheduler
import java.awt.Color

class State : BotFunction {

    override val order = "state"

    override fun accept(event: TokenizedMessageReceivedEvent) {

        answerEmbed(event) {
            it.setColor(Color.CYAN)
            it.addField("looping", event.musicManager.scheduler.loop.toString(), true)
            it.addField("shuffling", event.musicManager.scheduler.shuffle.toString(), true)
            it.addField("paused", event.musicManager.player.isPaused.toString(), true)

            if (event.musicManager.scheduler.notPlayed.isEmpty()) {
                it.addField("left to play", "nothing", false)
            } else {
                it.addField("left to play", event.musicManager.scheduler.notPlayed.joinToString(", ") { f -> PlaybackScheduler.readableName(f) }, false)
            }
        }
    }

    override fun description(): String {
        return paddedOrder(order) + "- displays what the music player currently does"
    }
}
