package ambiance.bot.function.music

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction

class Shuffle : BotFunction {

    override val order = "shuffle"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        event.musicManager.scheduler.shuffle = java.lang.Boolean.parseBoolean(event.tokens[1])
    }

    override fun description(): String {
        return paddedOrder("$order [true|false]") + "- activates/deactivates shuffling"
    }
}
