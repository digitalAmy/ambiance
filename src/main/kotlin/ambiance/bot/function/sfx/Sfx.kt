package ambiance.bot.function.sfx

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction
import ambiance.main.SOUND_EFFECTS

class Sfx : BotFunction {

    override fun description(): String = paddedOrder("$order [path]") + "- play sound effect"

    override val order = "sfx"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        val effect = SOUND_EFFECTS[event.tokens[1]]
        if (effect != null) {
            event.musicManager.scheduler.playEffect(effect)
        }
    }
}