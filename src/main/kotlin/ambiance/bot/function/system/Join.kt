package ambiance.bot.function.system

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction
import ambiance.music.AmbianceAudioProvider

class Join : BotFunction {

    override val order = "join"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        event.base.member.get().voiceState.map { it.channel.block() }.subscribe { it.join { channel -> channel.setProvider(AmbianceAudioProvider(event.musicManager.player)) }.block() }
    }

    override fun description(): String {
        return paddedOrder(order) + "- asks bot to join voice channel the caller currently is in"
    }
}
