package ambiance.bot.function.resulthandler

import ambiance.main.LOG
import ambiance.music.PlaybackScheduler
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import discord4j.core.DiscordClient
import discord4j.core.`object`.presence.Activity
import discord4j.core.`object`.presence.Presence

class PlayImmediateResultHandler(private val client: DiscordClient, private val player: AudioPlayer) : AudioLoadResultHandler {

    override fun trackLoaded(track: AudioTrack) {
        LOG.info("found the track; will append")

        player.startTrack(track, false)
        client.updatePresence(Presence.online(Activity.playing(PlaybackScheduler.readableName(track))))
    }

    override fun playlistLoaded(playlist: AudioPlaylist) {
        // unused
    }

    override fun noMatches() {
        // base.getChannel().sendMessage("could not find resource " + resource);
    }

    override fun loadFailed(exception: FriendlyException) {
        LOG.info("could not open resource", exception)
    }
}
