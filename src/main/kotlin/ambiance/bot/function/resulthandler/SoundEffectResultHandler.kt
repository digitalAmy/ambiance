package ambiance.bot.function.resulthandler

import ambiance.main.LOG
import ambiance.main.SOUND_EFFECTS
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist
import com.sedmelluq.discord.lavaplayer.track.AudioTrack

class SoundEffectResultHandler : AudioLoadResultHandler {

    override fun loadFailed(exception: FriendlyException?) {
        LOG.info("could not open resource", exception)
    }

    override fun trackLoaded(track: AudioTrack?) {
        if (track != null) {
            SOUND_EFFECTS[track.identifier.split('/').last()] = track
        }
    }

    override fun noMatches() {
        // unused
    }

    override fun playlistLoaded(playlist: AudioPlaylist?) {
        // unused
    }
}