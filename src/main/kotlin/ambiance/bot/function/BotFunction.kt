package ambiance.bot.function

import ambiance.bot.TokenizedMessageReceivedEvent
import discord4j.core.`object`.entity.Member
import discord4j.core.spec.EmbedCreateSpec
import java.util.function.Consumer

interface BotFunction : Consumer<TokenizedMessageReceivedEvent> {

    fun description(): String

    fun paddedOrder(order: String) = order.padEnd(DESCRIPTION_PAD, ' ')

    fun answer(event: TokenizedMessageReceivedEvent, message: String) {
        event.base.message.channel.subscribe {
            it.createMessage("${mention(event.base.member.get())} $message").subscribe()
        }
    }

    fun answerEmbed(event: TokenizedMessageReceivedEvent, embedModifier: (EmbedCreateSpec) -> Unit) {
        event.base.message.channel.subscribe {
            it.createEmbed(embedModifier).subscribe()
        }
    }

    fun mention(member: Member): String {
        return "<@${member.id.asLong()}>"
    }

    val order: String

    companion object {
        const val DESCRIPTION_PAD = 24
    }
}
