package ambiance.bot.function.player

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction
import ambiance.main.PLAYERS

class Unregister : BotFunction {

    override fun description(): String {
        return paddedOrder(order) + "- unregister from games with this bot; deletes all tracked data"
    }

    override val order = "unregister"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        val member = event.base.member.get()
        val player = PLAYERS.find { it.id == member.id.asLong() }
        if (player != null) {
            PLAYERS.remove(player)
            answer(event, "unregistered successfully")
        } else {
            answer(event, "was not registered")
        }
    }
}