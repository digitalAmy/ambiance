package ambiance.bot.function.player

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction
import ambiance.main.PLAYERS

class PlayerInfo : BotFunction {

    override fun description(): String {
        return paddedOrder(order) + "- output you current game statistics"
    }

    override val order = "playerinfo"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        val member = event.base.member.get()
        val player = PLAYERS.find { it.id == member.id.asLong() }
        if (player != null) {
            answerEmbed(event) {
                it.addField("deaths", player.deathCounterData.deaths.toString(), false)
            }
        } else {
            answer(event, "is not registered with this bot")
        }
    }
}