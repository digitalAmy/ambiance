package ambiance.bot.function.player

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction
import ambiance.entities.DEATH_MESSAGES
import ambiance.main.PLAYERS
import kotlin.random.Random

class Death : BotFunction {

    private val random = Random(63474)

    override fun description(): String {
        return paddedOrder(order) + "- mark one death for yourself on the death counter"
    }

    override val order = "death"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        val member = event.base.member.get()
        val player = PLAYERS.find { it.id == member.id.asLong() }
        if (player != null) {
            player.deathCounterData.deaths += 1
            answer(event, DEATH_MESSAGES[random.nextInt(DEATH_MESSAGES.size)])
        } else {
            answer(event, "is not registered for games")
        }
    }
}