package ambiance.bot.function.player

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.bot.function.BotFunction
import ambiance.entities.Player
import ambiance.main.PLAYERS

class Register : BotFunction {

    override fun description(): String {
        return paddedOrder(order) + "- register for games with this bot; tracks some necessary data"
    }

    override val order = "register"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        val member = event.base.member.get()
        if (PLAYERS.none { it.id == member.id.asLong() }) {
            PLAYERS.add(Player(member.id.asLong()))
            answer(event, "registered successfully")
        } else {
            answer(event, "is already registered")
        }
    }
}