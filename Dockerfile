FROM openjdk:11-slim

COPY build/install/ambiance /home/ambiance/ambiance
COPY music /home/ambiance/music
COPY soundEffects /home/ambiance/soundEffects

CMD ["/home/ambiance/ambiance/bin/ambiance", "INSERT_TOKEN_HERE", "/home/ambiance/music", "/home/ambiance/soundEffects"]