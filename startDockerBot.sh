#!/usr/bin/env bash
gradle installDist

docker build --rm -t ambiance .
docker run --rm --name ambiance -it ambiance